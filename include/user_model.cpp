#ifndef USER_MODEL
#define USER_MODEL

#define GLEW_STATIC 1   // This allows linking with Static Library on Windows, without DLL
#include <GL/glew.h>    // Include GLEW - OpenGL Extension Wrangler
#include <GLFW/glfw3.h> // cross-platform interface for creating a graphical context,
                        // initializing OpenGL and binding inputs
#include <glm/glm.hpp>  // GLM is an optimized math library with syntax to similar to OpenGL Shading Language
#include <glm/gtc/matrix_transform.hpp> // include this to create transformation matrices
#include <glm/common.hpp>
#include <glm/gtc/type_ptr.hpp>

std::vector< UnitModel > seven_seg_model(int segFlags[7], int shaderProgram, int vertices, GLuint  VAO, GLuint textureID){

    float gridUnit = 1.0f;

    float width = gridUnit * 0.20f / 8;
    float depth = gridUnit * 0.20f /8;
    float radius = gridUnit * 0.20f /8;
    float height = gridUnit * 0.8f / 8;

    std::vector< UnitModel > modeList;

    // a
    if(segFlags[0] == 1){
        mat4 modelMatrix = translate(mat4(1.0f), vec3(0.0f, height * 3 , 0.0f)) * rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f)) * scale(mat4(1.0f), vec3(width, height, depth));
        UnitModel model = UnitModel(shaderProgram, modelMatrix, vertices, VAO, textureID);
        modeList.push_back(model);
    }

    // b
    if(segFlags[1] == 1){
        mat4 modelMatrix = translate(mat4(1.0f), vec3(height, height + (height), 0.0f))  * scale(mat4(1.0f), vec3(radius , height, radius ));
        UnitModel model = UnitModel(shaderProgram, modelMatrix, vertices, VAO, textureID);
        modeList.push_back(model);
    }

    // c
    if(segFlags[2] == 1){
        mat4 modelMatrix = translate(mat4(1.0f), vec3(height, 0, 0.0f))  * scale(mat4(1.0f), vec3(radius , height, radius ));
        UnitModel model = UnitModel(shaderProgram, modelMatrix, vertices, VAO, textureID);
        modeList.push_back(model);    }

    // d
    if(segFlags[3] == 1){
        mat4 modelMatrix = translate(mat4(1.0f), vec3(0.0f, -height, 0.0f)) * rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f)) * scale(mat4(1.0f), vec3(width, height, depth));
        UnitModel model = UnitModel(shaderProgram, modelMatrix, vertices, VAO, textureID);
        modeList.push_back(model);    }

    // e
    if(segFlags[4] == 1){
        mat4 modelMatrix = translate(mat4(1.0f), vec3(- height, 0, 0.0f))  * scale(mat4(1.0f), vec3(radius , height, radius ));
        UnitModel model = UnitModel(shaderProgram, modelMatrix, vertices, VAO, textureID);
        modeList.push_back(model);    }

    // f
    if(segFlags[5] == 1){
        mat4 modelMatrix = translate(mat4(1.0f), vec3(- height, height + (height), 0.0f))  * scale(mat4(1.0f), vec3(radius , height, radius ));
        UnitModel model = UnitModel(shaderProgram, modelMatrix, vertices, VAO, textureID);
        modeList.push_back(model);    }

    // g
    if(segFlags[6] == 1){
        mat4 modelMatrix = translate(mat4(1.0f), vec3(0.0f, height, 0.0f)) * rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f)) * scale(mat4(1.0f), vec3(width, height, depth));
        UnitModel model = UnitModel(shaderProgram, modelMatrix, vertices, VAO, textureID);
        modeList.push_back(model);    }

    return modeList;
}

struct LetterIDModel {
    std::vector< UnitModel > m_model_list;

    // params
    float scale = 0.7f;
    float x = 0.0f;
    float y = 0.0f;
    float z = 0.0f;
    float angle = 1.0f;

    LetterIDModel(int segFlags[7], int shaderProgram, int vertices, GLuint  VAO, GLuint textureID){
        m_model_list = seven_seg_model(segFlags, shaderProgram, vertices, VAO, textureID);

    }
};

#endif
