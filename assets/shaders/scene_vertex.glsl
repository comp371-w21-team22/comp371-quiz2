#version 330 core

uniform vec3 view_position;

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aUV; // NEW: UVs

uniform mat4 worldMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 light_view_proj_matrix;

out vec3 fragment_normal;
out vec3 fragment_position;
out vec4 fragment_position_light_space;

out vec3 vertexNormal;
out vec2 vertexUV; // NEW: output UVs

void main()
{
    fragment_normal = mat3(worldMatrix) * aNormal;
    fragment_position = vec3(worldMatrix * vec4(aPos, 1.0));
    fragment_position_light_space = light_view_proj_matrix * vec4(fragment_position, 1.0);
    gl_Position = projectionMatrix * viewMatrix * worldMatrix * vec4(aPos, 1.0);

    vertexNormal = aNormal;
    vertexUV = aUV;
}
