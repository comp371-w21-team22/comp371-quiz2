 COMP-371 Quiz-2 : Philippe Vo (27759908)

## What is it about ?
- This is a quiz for the COMP-371 class (Computer Graphics). 

## Bitbucket link
- You can find this project in "comp371-w21-team22/comp371-quiz2" on bitbucket

### Quiz-2
- The second quiz has us make a simulation environement where there is a stage, totem like model and axis.

- The models are made using the notion of "seven segment display". (seven_segment_display())
![seven segment display](seven.png)

- Using keyboard and mouse inputs, the user is able to move around in the scene.

## Mouse and Keyboard Inputs
```
- ESC                         : close window
- AWSD                         : move left-right and forward-backward
- R                         : switch to main camera
- M                         : switch to camera placed in front of model
- B                         : switch to camera placed behind of model
- Left-Right Arrow                         : switch to camera circling the center stage (press on left key first to get it going)
- X                         : switch to camera located at the main light source (main spotlight)
- 1                         : turn on main light
- 2                         : turn off main light
```
## Compile and Run Instructions (taken from the Lab06 readme.md instructions)
- please refer to "compile_instructions.md"

## Credits
### Quiz-2
- The second quiz borrows heavily from the "COMP-371 class" Lab04, Lab06 and Lab08. Some code was taken from there. It also borrows
heavily from the second assignment of "COMP-371".

## Resources used
- https://learnopengl.com/Getting-started/OpenGL

## Known Bugs and differences from quiz instructions given
- the skybox is 50 units because setting it at 75, strange black trianlges appear on the skybox.

## NOTES :
- to initiate the camera circling around, have to press the left arrow key first, then you can use both to circle around.
